#ifndef SHAPEAUX_H
/*
** Shapeaux.h
**  _______ __                       _______       ___ ___
** |   _   |  |--.---.-.-----.-----.|   _   .--.--(   Y   )
** |   1___|     |  _  |  _  |  -__||.  1   |  |  |\  1  /
** |____   |__|__|___._|   __|_____||.  _   |_____|/  _  \
** |:  1   |           |__|         |:  |   |     /:  |   \
** |::.. . |                        |::.|:. |    (::. |:.  )
** `-------'                        `--- ---'     `--- ---'
**
** The engine for the Shape The Pixel app.
** Shapeaux is where the logic happens and the app state is kept and transformed.
**
** Let's build the Shape The Pixel engine together. It's easier than you think.
** For starters, everything happens in this file. There are no external
** dependencies other than the C standard libraries.
**
** We'll start by exposing all of the Shapeaux public API inside the SHAPEAUX_H
** define. The implementation of these functions follows after, under the
** SHAPEAUX_IMPLEMENTATION #ifdef.
**
*/
/*-- MARK: The Shapeaux Public API (Forward-Declarations) --------------------*/
#define SHAPEAUX_H (1)
#include <stddef.h> // size_t
#include <stdbool.h> // bool

/*
** Care was taken to provide for Swift compatibility in this C code. Some
** public API functions make use of the SWIFT macro that allows their names and
** usage to be set for Swift.
** For more info about this macro see:
** https://github.com/apple/swift/blob/main/docs/CToSwiftNameTranslation.md
** And here it is:
*/
#define SWIFT(n) __attribute__((swift_name(n)))

/*
** The `id_t` type.
**
** Most objects in Shapeaux are identifiable and make use of this `id_t` type.
** It is composed of a single `.value` attribute, a char[SHAPEAUX_ID_SIZE]
** fixed-sized array (this .value attribute is then mapped to .code in Swift,
** because there is no support for fixed-sized arrays in the language).
**
** Shapeaux does not create id's, instead it expects them to be provided by
** external code. Currently in "Shape The Pixel" the id's are being done using
** the `cuid()` method (see: http://usecuid.org). The `id_t` declared here
** holds enough space to accommodate a cuid() string.
**
** This struct is exported to Swift to be used as `Id(code: "some id value")`
*/
typedef struct id_t id_t;
struct id_t {
#define SHAPEAUX_ID_SIZE (42)
    char value[SHAPEAUX_ID_SIZE];
} SWIFT("Id");

/*
** This method creates an `id_t` and copies the provided `code` string into
** the `.value` of the resulting id.
**
** This is the function used by the Swift `Id.init(code:)` constructor.
** The `id_t` returned by this function can then be used in the Shapeaux
** functions that need a unique id.
*/
SWIFT("Id.init(code:)")
id_t shapeaux_create_id(char const * const code);

/*
** This function is a getter for the `.value` of an `id_t`.
**
** It returns the fixed-sized array `char[SHAPEAUX_ID_SIZE]` as a constant char*.
** This getter is useful because Swift does not support fixed-sized arrays.
**
** Swift makes use of this method when accessing the `.code` attribute of an
** `Id()`.
*/
SWIFT("getter:Id.code(self:)")
char const * shapeaux_id_string(id_t const * const some_id);

/*
** The method that compares two ids for equality.
** It works by traversing their `.value` char arrays until a difference
** is found.
** It is available in Swift `Id()` as `someId.isEqual(to: someOtherId)`.
*/
SWIFT("Id.isEqual(self:to:)")
bool shapeaux_id_equal(id_t id1, id_t id2);

/*
** The `shape_t` type.
**
** A shape represents a grid element. Inside each grid element there is a shape.
** These shapes can be edited and changed. A shape can be empty, which means
** that it was not yet edited, it maps to a transparent grid element.
** Each shape has a unique Id by which it can be identified.
*/
typedef struct shape_t shape_t;

/*
** The `shape_kind_t` represents the possible types a shape can be.
** Right now a shape can either be empty or a set of geometric elements that
** make up its content.
*/
typedef enum shape_kind_t {
    SHAPE_EMPTY,
    SHAPE_GEOMETRIC,
} shape_kind_t;

/*
** A `shape_t` is at minimum just an Id and its type.
** If the type is not SHAPE_EMPTY then it is expected that `shape_t` has the
** needed data for it to be edited and represented outside of the editor.
** TODO: Shape has no editor or representation data
*/
struct shape_t {
    id_t shape_id;
    shape_kind_t type;
} SWIFT("Shape");

/*
** A simple getter for the shape `id`. It returns the id string, which is
** useful for Swift to quickly access a shape id.
*/
SWIFT("getter:Shape.id(self:)")
char const * shapeaux_shape_id(shape_t const * const some_shape);
#endif /* SHAPEAUX_H */

/*-- MARK: The Shapeaux Implementation ---------------------------------------*/
#ifdef SHAPEAUX_IMPLEMENTATION
#define SHAPEAUX_IMPLEMENTATION_INCLUDED (1)
/*
**
** The following code implements the functions listed above and provides
** all the helper methods and structs they need to work as expected.
**
*/
/*-- MARK: The shape_t Implementation ----------------------------------------*/

/*
** The getter for the shape `id`. This is useful to allow Swift to access the
** shape id at the `.id` accessor. Otherwise it would have to do
** be read at the `shape.shape_id.code` in Swift.
** It returns the fixed-array id as a `char const *`.
*/
SWIFT("getter:Shape.id(self:)")
char const *
shapeaux_shape_id(shape_t const * const some_shape) {
    return some_shape->shape_id.value;
}

/*-- MARK: The id_t Implementation -------------------------------------------*/
/*
** The id_t external constructor. This function exists to allow external code
** to create Shapeaux id_t's.
*/
SWIFT("Id.init(code:)")
id_t
shapeaux_create_id(char const * const code) {
    id_t result = { .value = {0} };
    // Copy the provided string into the result.value position
    for(size_t pos = 0; pos < SHAPEAUX_ID_SIZE; ++pos) {
        result.value[pos] = code[pos];
        // Finish the copy if the provided string is at the end
        if (code[pos] == 0) {
            break;
        }
        // If the provided string is too big to fit in the SHAPEAUX_ID_SIZE
        // char array, then finish the copy by placing a NULL at the end of the
        // result.value array
        if (pos == SHAPEAUX_ID_SIZE - 1) {
            result.value[pos] = 0;
        }
    }
    return result;
}

/*
** This function is just a getter that returns the fixed-sized string array of
** the provided `id_t` as a `char const * const`.
*/
SWIFT("getter:Id.code(self:)")
char const *
shapeaux_id_string(id_t const * const some_id) {
    return some_id->value;
}

/*
** Method that checks if two ids are equal.
** It traverses the fixed-sized array up until a different char is found.
*/
SWIFT("Id.isEqual(self:to:)")
bool
shapeaux_id_equal(id_t id1, id_t id2) {
    for(size_t pos = 0; pos < SHAPEAUX_ID_SIZE; ++pos) {
        if (id1.value[pos] != id2.value[pos]) {
            return false;
        }
        // The end is reached before the SHAPEAUX_ID_SIZE is fully occupied.
        // If this point is reached it means that
        // both strings are equal but do not fill the whole .value array.
        if (id1.value[pos] == 0 || id2.value[pos] == 0) {
            return true;
        }
    }
    // Return true because the contents of the arrays might not be
    // null terminated for this to happen inside the loop.
    return true;
}

#endif /* SHAPEAUX_IMPLEMENTATION */
