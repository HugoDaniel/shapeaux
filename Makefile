build:
	swift build -Xcc -Weverything -Xcc -Werror -Xcc -std=c17
/usr/local/bin/xcpretty:
	gem install xcpretty
test: /usr/local/bin/xcpretty
	swift test -Xcc -Wall -Xcc -Werror -Xcc -std=c17 2>&1 >/dev/null | xcpretty -rjunit
#	swift test -Xcc -Wall -Xcc -Werror -Xcc -std=c17 -Xcc -DSHAPEAUX_IMPLEMENTATION=1 
