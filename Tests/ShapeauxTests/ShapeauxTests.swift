import XCTest

import Shapeaux

final class ShapeauxTests: XCTestCase {
    override class var defaultMetrics: [XCTMetric] {
      [
        XCTClockMetric(),
        XCTCPUMetric(limitingToCurrentThread: true),
        XCTMemoryMetric(),
        XCTStorageMetric(),
      ]
    }
    /* MARK: Shape() TESTS */
    func testShapeExists() {
        XCTAssertEqual(String(describing: type(of: Shape())), "shape_t")
    }
    func testShapeStartsWithNoShapeKind() {
        let s = Shape()
        XCTAssertEqual(s.type, SHAPE_EMPTY)
        XCTAssertFalse(s.type == SHAPE_GEOMETRIC)
    }
    func testShapeStartsWithEmptyId() {
        let s = Shape()
        XCTAssertEqual(String(validatingUTF8: s.id), "")
    }
    /* MARK: Id() TESTS */
    func testIdExists() {
        XCTAssertEqual(String(describing: type(of: Id())), "id_t")
    }
    func testIdHasDefaultValue() {
        let id = Id()
        XCTAssertEqual(String(validatingUTF8: id.code), "")
    }
    func testIdCanStartWithAValue() {
        let id = Id(code: "Something")
        XCTAssertEqual(String(validatingUTF8: id.code), "Something")
    }
    func testIdHasASizeLimit() {
        let id = Id(code: "SomethingVeryLongSomethingVeryLongSomethingVeryLongSomethingVeryLongSomethingVeryLongSomethingVeryLongSomethingVeryLong")
        // Truncates to the value defined by the macro ID_SIZE
        // Which is 42 at the moment, this is 41 chars + NULL
        XCTAssertEqual(String(validatingUTF8: id.code), "SomethingVeryLongSomethingVeryLongSomethi")
    }
    func testIdHasEqualMethod() {
        let id = Id()
        XCTAssertEqual(String(describing: type(of: id.isEqual)),
                       "(id_t) -> Bool")
    }
    func testIdCanBeCompared() {
        let id1 = Id(code: "Something1")
        let id2 = Id(code: "Something2")
        let id3 = Id(code: "Something1")
        let empty1 = Id()
        let empty2 = Id()
        XCTAssert(!id1.isEqual(to: id2), "id1 must NOT be equal to id2")
        XCTAssert(id1.isEqual(to: id1), "id1 must be equal to id1")
        XCTAssert(id3.isEqual(to: id1), "id3 must be equal to id1")
        XCTAssert(empty1.isEqual(to: empty2), "empty id's are equal")
    }
    func testIdComparisonPerformance() {
        let id1 = Id(code: "SomethingVeryLongSomethingVeryLongSomethi")
        let id2 = Id(code: "SomethingVeryLongSomethingVeryLongSometh0")
        measure(metrics: Self.defaultMetrics) {
            _ = id1.isEqual(to: id2)
        }
    }
}
